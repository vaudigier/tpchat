import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Channel} from '../models/channel';
import {environment} from '../../environments/environment';

const url = '/channels';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  constructor(private http: HttpClient) {
  }

  getChannels(): Observable<Channel[]> {
    return this.http.get<Channel[]>(`${environment.backUrl}${url}`);
  }

  getChannel(channel: Channel): Observable<Channel> {
    return this.http.get<Channel>(`${environment.backUrl}${url}/${channel.id}`);
  }
}
