import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Message} from '../models/message';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

const url = '/messages';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) {
  }

  createMessageByChannel(channelId: string, message: Message): Observable<Message> {
    return this.http.post<Message>(`${environment.backUrl}/channels/${channelId}${url}`, message);
  }

  getMessagesByChannel(channelId: string): Observable<Message[]> {
    return this.http.get<Message[]>(`${environment.backUrl}/channels/${channelId}${url}`);
  }
}
