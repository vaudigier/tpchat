import {Component, OnInit} from '@angular/core';
import {MessageService} from '../../services/message.service';
import {Message} from '../../models/message';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {

  idChannel: string;
  messages: Message[];
  newText: string;

  private updateSubscription: Subscription;

  constructor(private messageService: MessageService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.idChannel = params.get('id');
    });
    this.updateSubscription = interval(1000).subscribe(
      () => this.loadMessage()
    );
  }

  loadMessage() {
    this.messageService.getMessagesByChannel(this.idChannel).subscribe(
      (messages: Message[]) => this.messages = messages
    );
  }

  createMessage() {
    const newMessage = {
      author: localStorage.getItem('pseudo'),
      text: this.newText
    };

    this.messageService.createMessageByChannel(this.idChannel, newMessage).subscribe(
      (message: Message) => this.messages.push(message)
    );

    this.newText = '';
  }

}
