import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  pseudo: string;

  constructor(private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }

  connect() {
    localStorage.setItem('pseudo', this.pseudo);
    this.router.navigate(['/channels']);
  }
}
