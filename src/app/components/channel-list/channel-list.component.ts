import {Component, OnInit} from '@angular/core';
import {Channel} from '../../models/channel';
import {ChannelService} from '../../services/channel.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.css']
})
export class ChannelListComponent implements OnInit {

  channels: Channel[];

  constructor(private channelService: ChannelService,
              private router: Router) {
  }

  ngOnInit() {
    this.loadChannels();
  }

  loadChannels() {
    this.channelService.getChannels().subscribe(
      (channels: Channel[]) => this.channels = channels
    );
  }

  changeChannel(newChannel: Channel) {
    this.channelService.getChannel(newChannel).subscribe(
      (channel: Channel) => this.router.navigate([`channels/${channel.id}`])
    );
  }

}
